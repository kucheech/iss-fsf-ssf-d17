(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["MyAppService"];

    function MyAppCtrl(MyAppService) {
        var self = this; // vm
        self.name = "";
        self.email = "";
        self.reply = "";

        self.register = function () {

            // MyAppService.test();

            var body = {
                name: self.name,
                email: self.email
            };

            MyAppService.postAsForm("/register", body)
                .then(function (result) {
                    console.log(result);
                }).catch(function (err) {
                    console.log(err);
                });
        };

        // self.register = function () {
        //     $http.post({
        //         method: "POST",
        //         url: "/register",
        //         headers: {
        //             "Content-Type": "application/x-www-form-urlencoded"
        //         },
        //         data: $httpParamSerializerJQLike({
        //                 name: self.name,
        //                 email: self.email
        //             })
        //     }).then(function (result) {
        //         console.log(result);
        //     }).catch(function (err) {
        //         console.log(err);
        //     });
        // }

        // self.register = function () {
        //     $http.post("/register", {
        //         name: self.name,
        //         email: self.email
        //     }).then(function (result) {
        //         console.log(result);
        //     }).catch(function (err) {
        //         console.log(err);
        //     });
        // }

        self.initForm = function () {
            // $http.get("/hello")
            //     .then(function (result) {
            //         console.log(result);
            //         self.reply = result.data;
            //     }).catch(function (e) {
            //         console.log(e);
            //     });
            // MyAppService.getImages("hello")
            //     .then(function (result) {
            //         console.log(result);
            //         self.reply = result.data;
            //     }).catch(function (e) {
            //         console.log(e);
            //     });

            MyAppService.test();
        };

        // self.initForm();
    }

})();