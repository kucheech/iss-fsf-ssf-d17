(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q", "$httpParamSerializerJQLike"];
    // MyAppService.$inject = ["$http", "$q"];


    function MyAppService($http, $q, $httpParamSerializerJQLike) {
        var service = this;

        //expose the following services
        service.getImages = getImages;
        service.postAsJSON = postAsJSON;
        service.postAsForm = postAsForm;
        service.test = test;

        function test() {
            console.log("test");
        }

        function postAsJSON(url, body) {
            console.log("postAsJSON");
            var defer = $q.defer();

            $http({
                method: "POST",
                url: url,
                data: body
            }).then(function (result) {
                console.log(result);
                defer.resolve(result);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function postAsForm(url, body) {
            var defer = $q.defer();
            $http({
                method: "POST",
                url: url,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                data: $httpParamSerializerJQLike(body)
            }).then(function (result) {
                console.log(result);
                defer.resolve(result);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getImages(tag) {

            var defer = $q.defer();

            $http.get("https://api.giphy.com/v1/gifs/search", {
                params: {
                    api_key: "ac419443623246189d35e51eab60c1d9",
                    q: tag,
                    limit: 5,
                    offset: 0
                }
            }).then(function (result) {
                console.log(result.data.data[0]);
                var data = result.data.data;
                var images = []
                for (var i in data) {
                    images.push(data[i].images.fixed_height_small.url);
                    // images.push(data[i].images.downsized_medium.url);      
                }
                // var tag = self.tag;
                defer.resolve(images);
            }, function (err) {
                deferred.reject(err);
            });

            return defer.promise;
        }

    }

})();