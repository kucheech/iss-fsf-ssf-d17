const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

const app = express();
app.use(bodyParser.urlencoded({ extended: false })); //Content-Type: application/x-www-form-urlencoded
app.use(bodyParser.json()); //Content-Type: application/json

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});

// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

const SELECT_ALL_USERS = "select * from users";

const getAllUsers = mkQuery(SELECT_ALL_USERS, pool);


const mkLogger = function (prefix) {
    return function (req, res, next) {
        console.info("[%s] Method %s, Resource %s", prefix, req.method, req.originalUrl);
        next();
    };
};

app.use(mkLogger("ALL"));


const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

app.use(function (req, res, next) {
    console.info("Method %s, Resource %s", req.method, req.originalUrl);
    next(); //use this to forward to next handler
});

app.get("/time", function (req, res) {
    res.status(200).type("text/html").send("<h1>The current time is " + new Date() + "</h1>");
});

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});


app.post("/register",

    bodyParser.urlencoded({ extended: false }),

    function (req, res) {
        
        var user = req.body;
        console.log(req);
        res.status(200).send("Thanks " + user.name + " for posting. A confirmation email will be sent to " + user.email);
    });

app.get("/users", function (req, res) {

    getAllUsers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No users");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});


// app.get("/users", function (req, res) {
//     // console.log("/users");

//     pool.getConnection(function (err, conn) {
//         if (err) {
//             conn.release();
//             res.status(500).send(err);
//         }

//         const SQL_QUERY = "SELECT * FROM users";
//         conn.query(SQL_QUERY, function (err, rows, fields) {
//             if (err) {
//                 conn.release();
//                 console.log(err);
//                 res.status(500).send(err);
//                 throw err;
//             }

//             console.log(rows);
//             // console.log(fields);

//             res.status(200).send(rows);
//             conn.release();
//         });
//     });

// });




// app.get("/users", function (req, res) {
//     // console.log("/users");
//     var deferred = q.defer();

//     pool.getConnection(function (err, conn) {
//         if (err) {
//             conn.release();
//             deferred.reject(res.status(500).send(err));
//         }

//         const SQL_QUERY = "SELECT * FROM users";
//         conn.query(SQL_QUERY, function (err, rows, fields) {
//             if (err) {
//                 conn.release();
//                 console.log(err);
//                 deferred.reject(res.status(500).send(err));
//                 throw err;
//             }

//             console.log(rows);
//             // console.log(fields);

//             deferred.resolve(res.status(200).send(rows));
//             conn.release();
//         });
//     });

//     return deferred.promise;
// });



//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app