(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["MyAppService"];

    function MyAppCtrl(MyAppService) {
        var self = this; // vm
        self.name = "";
        self.email = "";
        self.reply = "";
        self.message = "";
        self.showResult = false;

        self.register = function () {

            // MyAppService.test();

            var body = {
                name: self.name,
                email: self.email
            };

            MyAppService.postAsForm("/register", body)
                .then(function (result) {
                    // console.log(result);
                    self.message = result.data;
                    self.showResult = true;                    
                }).catch(function (err) {
                    console.log(err);
                    self.message = err;
                    self.showResult = true; 
                });
        };

        // self.register = function () {
        //     $http.post({
        //         method: "POST",
        //         url: "/register",
        //         headers: {
        //             "Content-Type": "application/x-www-form-urlencoded"
        //         },
        //         data: $httpParamSerializerJQLike({
        //                 name: self.name,
        //                 email: self.email
        //             })
        //     }).then(function (result) {
        //         console.log(result);
        //     }).catch(function (err) {
        //         console.log(err);
        //     });
        // }

        // self.register = function () {
        //     $http.post("/register", {
        //         name: self.name,
        //         email: self.email
        //     }).then(function (result) {
        //         console.log(result);
        //     }).catch(function (err) {
        //         console.log(err);
        //     });
        // }

        self.initForm = function () {
            // $http.get("/hello")
            //     .then(function (result) {
            //         console.log(result);
            //         self.reply = result.data;
            //     }).catch(function (e) {
            //         console.log(e);
            //     });
            // MyAppService.getImages("hello")
            //     .then(function (result) {
            //         console.log(result);
            //         self.reply = result.data;
            //     }).catch(function (e) {
            //         console.log(e);
            //     });

            MyAppService.test();
        };

        // self.initForm();
    }

})();